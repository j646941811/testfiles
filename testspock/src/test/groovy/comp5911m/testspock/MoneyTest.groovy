package comp5911m.testspock


import spock.lang.*


class MoneyTest extends Specification {

    def oneFifty = new Money(1, 50)

    def "creating a Money"() {
        expect:
        oneFifty.getEuros() == 1
        oneFifty.getCents() == 50
    }

    def "creating a Money with cents too low"() {
        when: new Money(1, -1)
        then: thrown(IllegalArgumentException)
    }

    def "representing Money as a string"() {
        setup:
        def oneFive = new Money(1, 5)

        expect:
        oneFifty.toString() == "\u20ac1.50"
        oneFive.toString() == "\u20ac1.05"
    }
}
